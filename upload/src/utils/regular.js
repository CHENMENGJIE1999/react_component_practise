const splitFileName = {
    reg: /(.*\/)*([^.]+).*/ig,
    msg: '截取文件名称'
};
const fileNameRegular = {
    reg: /^[a-zA-Z0-9\u4e00-\u9fa5]{1,50}.*$/,
    msg: '文件名只能由数字，字母和中文构成,且长度不超过50',
};
module.exports = {
    splitFileName,
    fileNameRegular
};
