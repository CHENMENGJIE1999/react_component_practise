import React from 'react';
import {message} from 'antd';
import {fileNameRegular, splitFileName} from './regular';
/**
 *  上传文件之前的检查函数
 * @param file
 * @returns {Promise<any>}
 */
export function checkBeforeFileUpload(file) {
    return new Promise((resolve, reject) => {
        //  检测文件名是否存在
        if (!file) {
            console.log('请检查utils.js中函数checkBeforeFileUpload的参数file是否为空');
            return reject(false);
        }
        //  检测是否有多个'.'符号
        const pointAppearTimes = file.name.split('.').length - 1;
        // console.log(pointAppearTimes);
        if (pointAppearTimes != 1) {
            console.log('enter');
            message.error('The file format is incorrect!');
            return reject(false);
        }
        //  截取文件名称
        let fileName = file.name;
        fileName = fileName.replace(splitFileName.reg, '$2');

        //  检测文件名称是否只函数数字，字母和英文
        if (!fileNameRegular.reg.test(fileName)) {
            message.error(fileNameRegular.msg);
            return reject(false);
        }

        if (file.size / 1024 / 1024 > 50) {
            message.error('文件大小不允许超过50兆！！');
            return reject(false);
        }
        return resolve(true);
    });
}
