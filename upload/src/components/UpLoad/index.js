import React, {PureComponent} from 'react';
import {Upload, Row, Modal, Button, Icon} from 'antd';
import {checkBeforeFileUpload} from '../../utils/utils';
// import remoteLinkAddress from "../../utils/ip";
const {confirm} = Modal;
export default class UpLoadList extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            changeFlag: 0,
        };
    }

    componentWillReceiveProps() {
        this.setState({
            fileList: [],
            changeFlag: 0,
        })
    }


    handleChange = ({fileList}) => {
        const {getUploadData, upLoadApi, num} = this.props;
        this.setState({
            changeFlag: 1,
        })
        if (!num) {
            fileList = fileList.slice(-1);
        }
        fileList.map((value, index) => {
            if (value.response && value.response.code === 1) {
                value.status = 'error';
            }
        })
        let str = '';
        if (fileList.length !== 0) {
            fileList.map((value, index) => {
                //  新增加的文件
                if (value.response && value.response.code === 0 && value.status !== 'error') {
                    str += value.response.data;
                }
                //  原本就有的文件
                else if (value.status !== 'error') {
                    if (upLoadApi) {
                        str += value.uid;
                    }
                    str = value.originFileObj
                }
            });
            this.setState({
                attIds: str,
            })
        }
        this.setState({
            fileList: [...fileList]
        });
        if (getUploadData) {
            getUploadData(fileList, this.state.attIds, this.state.changeFlag)
        }

    };

    onRemoveFile = (file) => new Promise((resolve, reject) => {
        confirm({
            title: '是否删除附件',
            okText: '删除',
            cancelText: '取消',
            onOk: () => {
                this.handleRemoveFile(file);
                return resolve(true);
            },
            onCancel: () => reject(false)
        })
    });

    handleRemoveFile = (file) => {
        const {dataList} = this.props;
        console.log(dataList);
        const newDataList = [];
        if (dataList) {
            // const materialArr = dataList.attIds.split(';');
            // for (let i = 0; i < materialArr.length; i++) {
            //     if (materialArr[i] == file.uid) {
            //         materialArr.splice(i, 1);
            //         break;
            //     }
            // }
            // if (materialArr.length > 0) {
            //     dataList.attIds = materialArr.join(';');
            // }
            // else {
            //     dataList.attIds = '';
            // }
            //
            // for (let i = 0; i < dataList.attFiles.length; i++) {
            //     if (dataList.attFiles[i].id == file.uid) {
            //         dataList.attFiles.splice(i, 1);
            //         break;
            //     }
            // }

            for (let item of dataList) {
                if (item.uid !== file.uid) {
                    newDataList.push(item);
                }
            }
        }
        // return dataList;
        return newDataList;
    };

    render() {
        const {changeFlag, fileList} = this.state;
        const {files, setting, dataList, setting: {listType}} = this.props;
        const props = {
            ...setting,
            // fileList: changeFlag ? fileList : files,
            fileList: changeFlag ? fileList : dataList,
            onChange: this.handleChange,
            beforeUpload: checkBeforeFileUpload,
            onRemove: this.onRemoveFile,
        };

        return <Upload {...props}>
            {
                listType === 'picture-card' ?
                    <div>
                        <Icon type='plus'/>
                        <div style={{marginTop: 8}}>Upload</div>
                    </div>
                    :
                    <Button>
                        <Icon type="upload"/>上传附件
                    </Button>
            }
        </Upload>
    }
}
