import React from "react";
import {Upload, Button, Icon} from 'antd';
import UploadList from "antd/es/upload/UploadList";
import UpLoadList from "./components/UpLoad";

export default class Invoice extends React.Component {
    constructor(props) {
        super(props);
    }

    handleChange = ({file, fileList}) => {
        if (file.status !== 'uploading') {
            console.log(file, fileList);
        }
    }

    render() {
        const props = {
            action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
            onChange: this.handleChange,
            defaultFileList: [
                {
                    uid: '1',
                    name: 'xxx.png',
                    status: 'done',
                    response: 'Server Error 500', // custom error message to show
                    url: 'http://www.baidu.com/xxx.png',
                },
            ],
        };
        const uploadProps = {
            setting: {
                listType: 'picture-card',
            },
            dataList: [
                {
                    uid: '3',
                    name: 'zzz.png',
                    status: 'error',
                    response: 'Server Error 500', // custom error message to show
                    url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
                },
            ],
        }
        return (
            <>
                <div>
                    <h1>This is UpLoad</h1>
                </div>
                {/*<Upload {...props}>*/}
                {/*    <Button>*/}
                {/*        <Icon type="upload"/> Upload*/}
                {/*    </Button>*/}
                {/*</Upload>*/}
                <UpLoadList {...uploadProps}>
                    <Button type='primary'>
                        <Icon type="upload"/> Upload
                    </Button>
                </UpLoadList>
            </>
        )
    }
}
