export const formItemLayout = {
    labelCol : {
        xs : { span : 24 },
        sm : { span : 5 },
        md : { span : 5 },
    },
    wrapperCol : {
        xs : { span : 24 },
        sm : { span : 15 },
        md : { span : 15 },
    },
};
