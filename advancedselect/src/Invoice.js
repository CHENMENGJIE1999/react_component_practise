import React from "react";
import moment from 'moment'
import {Form, Input, Row, Col, Icon, Divider, Table, Tooltip, Button, Card,} from 'antd';

import AdvancedSelect from './components/AdvancedSelect/index'
import StandardTable from "./components/StandardTable";
import {formItemLayout} from "./utlis/globalUIConfig";
import {getUuid} from "./utlis/utlis";
import * as SelectFieldConfig from './utlis/globalSelectDataConfig';

const FormItem = Form.Item;
//语法糖，需要专门配置，但是暂时没解决
// @Form.create()

class Invoice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            invoiceRecords: [
                {
                    name: "Santa Monica",
                    number: 1995,
                    amount: "$10,800",
                    due: "12/05/1995",
                    product: 'milk',
                    uuid: getUuid()
                },
                {
                    name: "Stankonia",
                    number: 2000,
                    amount: "$8,000",
                    due: "10/31/2000",
                    product: 'fruit',
                    uuid: getUuid()

                },
                {
                    name: "Ocean Avenue",
                    number: 2003,
                    amount: "$9,500",
                    due: "07/22/2003",
                    product: 'meat',
                    uuid: getUuid()

                },
                {
                    name: "Tubthumper",
                    number: 1997,
                    amount: "$14,000",
                    due: "09/01/1997",
                    product: 'milk',
                    uuid: getUuid()

                },
                {
                    name: "Wide Open Spaces",
                    number: 1998,
                    amount: "$4,600",
                    due: "01/27/1998",
                    product: 'beans',
                    uuid: getUuid()

                },
                {
                    name: "Monica",
                    number: 1995,
                    amount: "$10,800",
                    due: "12/05/1995",
                    product: 'meat',
                    uuid: getUuid()
                },
            ],
            pagination: {
                pageNum: 1,
                pageSize: 10,
            },
            selectedRows: [],
            isFormCardVisible:false
        }
    }

    handleUpdateSelectedRows = (record) => {
        this.setState({
            selectedRows: record,
            isFormCardVisible:true
        })
    }
    createAction = (record) => {
        return (
            <>
                <div>
                    <Tooltip title='编辑'>
                        <a
                            onClick={() => this.handleUpdateSelectedRows(record)}
                        >
                            <Icon type='edit'/>
                        </a>
                    </Tooltip>
                </div>

            </>
        )
    }
    confirmUpdateInvoice = () => {
        const {invoiceRecords, selectedRows} = this.state;
        const {
            form: {validateFields}
        } = this.props;
        validateFields((error, values) => {
            if (!error) {
                const tmpInvoicesData = JSON.parse(JSON.stringify(invoiceRecords));
                for (let item of tmpInvoicesData) {
                    if (item.hasOwnProperty('uuid') && selectedRows.hasOwnProperty('uuid')) {
                        if (item.uuid === selectedRows.uuid) {
                            Object.assign(item, values)
                        }
                    }
                }
                console.log(tmpInvoicesData)
                this.setState({
                    invoiceRecords: tmpInvoicesData
                })
            }
        })
    }
    handleCancelSelected = () => {
        this.setState({
            selectedRows: []
        })
    }

    render() {
        const {
            form: {getFieldDecorator}
        } = this.props;
        const {
            invoiceRecords = [],
            pagination = {},
            selectedRows = [],
            isFormCardVisible=false
        } = this.state;
        const data = {
            list: invoiceRecords,
            pagination
        };
        const columns = [
            {
                title: 'uuid',
                key: 'uuid',
                dataIndex: 'uuid',
                width: '30%',
            },
            {
                title: 'No',
                key: 'index',
                dataIndex: 'index',
                width: '5%',
                render: (text, record, index) => <span>{index + 1}</span>
            },
            {
                title: '姓名',
                key: 'name',
                dataIndex: 'name',
                width: '10%',
            },
            {
                title: '产品',
                key: 'product',
                dataIndex: 'product',
                width: '10%',
            },
            {
                title: '数量',
                key: 'number',
                dataIndex: 'number',
                width: '10%',
            },
            {
                title: '总价',
                key: 'amount',
                dataIndex: 'amount',
                width: '10%',
            }, {
                title: '发票日期',
                key: 'due',
                dataIndex: 'due',
                width: '10%',
            }, {
                title: 'Action',
                key: 'action',
                align: 'center',
                width: '20%',
                render: (text, record) => (
                    <div>
                        {this.createAction(record)}
                    </div>
                )
            }
        ];
        const productData = [
            {
                text: 'meat',
                value: 'meat',
                key: 'meat',
            }, {
                text: 'fruit',
                value: 'fruit',
                key: 'fruit',
            }, {
                text: 'beans',
                value: 'beans',
                key: 'beans',
            }, {
                text: 'milk',
                value: 'milk',
                key: 'milk'
            }
        ];
        const {name = '', product = '', amount = '', due = '', number = ''} = selectedRows;
        return (
            <>
                <h1>This is AdvancedSelect</h1>
                <div style={{"display": "flex"}}>
                    <div style={{"width": "1000px"}}>
                        <StandardTable
                            selectedRows={selectedRows}
                            onSelectRow={this.handleSelectRows}
                            rowSelection={null}
                            data={data}
                            columns={columns}
                            onChange={() => {
                            }}
                            rowKey={r => r.uuid}
                        />
                    </div>
                    <div style={{"flex": 1}}>
                        {isFormCardVisible ?
                            (<Card bordered={false}>
                                <Form style={{marginTop: 8}}>
                                    <Row>
                                        <Col>
                                            <FormItem {...formItemLayout} label="姓名">
                                                {getFieldDecorator('name', {
                                                    initialValue: name || '',
                                                    rules: [
                                                        {
                                                            required: true,
                                                            whitespace: true,
                                                            max: 11,
                                                            min: 1,
                                                            message: '请正确姓名.不能只为空格，长度小于11个字',
                                                        },
                                                    ],
                                                })
                                                (<Input placeholder="不能只为空格，长度小于11个字"/>)
                                                }
                                            </FormItem>
                                        </Col>

                                    </Row>
                                    <Row>
                                        <Col span={12}>
                                            <FormItem {...formItemLayout} label='产品'>
                                                {getFieldDecorator('product', {
                                                    initialValue: product || '',
                                                    rules: [{
                                                        required: true,
                                                        message: '请选择置办产品'
                                                    }]
                                                })(
                                                    <AdvancedSelect
                                                        dataSource={productData}
                                                        type='DEFAULT'
                                                        onChange={() => {
                                                        }}
                                                        style={{width: 300}}
                                                        rowKey={r => r.uuid}
                                                    />
                                                )}
                                            </FormItem>
                                        </Col>
                                    </Row>
                                </Form>
                                <Row type="flex" justify="center">
                                    <Col>
                                        <Button
                                            type='primary'
                                            onClick={this.confirmUpdateInvoice}
                                        >
                                            确定
                                        </Button>

                                    </Col>
                                    <Col/>
                                    <Col>
                                        <Button
                                            onClick={this.handleCancelSelected}>取消</Button>
                                    </Col>
                                </Row>
                            </Card>) : ''}
                    </div>
                </div>
            </>

        )
    }
}

Invoice = Form.create({})(Invoice);
export default Invoice;
