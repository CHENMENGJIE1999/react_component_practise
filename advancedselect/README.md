# AdvancedSelect 组件练习demo
## 实现效果
利用`StandardTable`和`AdvancedSelect`实现数据的修改及渲染。
## 涉及知识点
- 条件渲染
- 列表渲染
- key值
- antd组件库的form表单
- 装饰器(可选)
## 注意事项
尽量选择以下版本安装`react`和`antd`,否则可能出现意料不到的情况，
其中`save`表示局部安装。
```javascript
yarn add react@^16.6.3 react-dom@^16.6.3
npm install --save antd@3.11.6
```

使用`antd`组件时,记得导入`antd`组件样式
```javascript
@import '~antd/dist/antd.css';
```
## 参考文档
react官网

https://reactjs.org/blog/2020/02/26/react-v16.13.0.html

antd官网

https://ant.design/docs/react/use-with-create-react-app-cn

