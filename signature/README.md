# Signature组件练习
## 组件说明
实现电子签名并生成签名图片,提交到父组件的图片可以直接在img中显示

## 涉及知识
- SignatureCanvas的使用
- props传参function
- ref使用

## 相关依赖
1. react-signature-canvas
2. resize-observer-polyfill

安装使用指令
```javascript
npm install react-signature-canvas --save

npm install resize-observer-polyfill --save-dev

```
## 注意事项
使用以下指令创建`react`项目
```javascript
yarn create react-app antd-demo
```
安装`React`和`antd`指定版本
```javascript
//安装react指定版本
yarn add react@^16.6.3 react-dom@^16.6.3

//安装antd指定版本
npm install --save antd@3.11.6
```
PS:建议使用`yarn`进行包的安装,否则可能出现意想不到的情况
## 参考文档
react官网

https://reactjs.org/blog/2020/02/26/react-v16.13.0.html

antd官网

https://ant.design/docs/react/use-with-create-react-app-cn

