组件说明
------
实现电子签名并生成签名图片,提交到父组件的图片可以直接在img中显示
![Alt text](./preview.png)

相关依赖
-----
1. react-signature-canvas
2. resize-observer-polyfill
3. antd 3.x

输入参数
-----
| 参数名称 | 是否必须 | 类型     | 说明                                            |
| ------ | ------ |--------|-----------------------------------------------|
| submitSign | 必须 | 函数     | 提交签名图片到父组件的函数                                 |

完整的参考范例
------
```javascript
<Signature submitSign={this.handleSubmitSign} />
```
其他信息
------
> 版本号:1.0
>
> 创建时间:2022/03/10
>
> 创建人:谢先博
