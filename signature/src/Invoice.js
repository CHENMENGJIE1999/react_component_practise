import React from "react";
import Signature from "./components/Signature";

class Invoice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            signImgUrl: ''
        }
    }

    handleSubmitSign = (imgUrl) => {
        console.log(imgUrl)
        this.setState({
            signImgUrl: imgUrl
        })
    }

    render() {
        return (
            <div>
                <h1>This is Signature</h1>
                <div>发票负责人签名:</div>
                <Signature submitSign={this.handleSubmitSign}/>
            </div>
        )
    }
}

export default Invoice;
