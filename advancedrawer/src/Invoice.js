import React from 'react';
import {Card, Tooltip, Icon, Space, Divider, Modal, Form, Button} from 'antd';
import {getUuid} from './utlis/utlis.js'
import {INVOICE_DETAIL_STATUS} from "./utlis/Enum";
import StandardTable from './components/StandardTable/index.js'
import AdvancedDrawer from "./components/AdvancedDrawer";
import InvoiceDetail from './InvoiceDetail'

class Invoice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            invoiceRecords: [
                {
                    name: "Santa Monica",
                    number: 1995,
                    amount: "$10,800",
                    due: "12/05/1995",
                    uuid: getUuid()
                },
                {
                    name: "Stankonia",
                    number: 2000,
                    amount: "$8,000",
                    due: "10/31/2000",
                    uuid: getUuid()

                },
                {
                    name: "Ocean Avenue",
                    number: 2003,
                    amount: "$9,500",
                    due: "07/22/2003",
                    uuid: getUuid()

                },
                {
                    name: "Tubthumper",
                    number: 1997,
                    amount: "$14,000",
                    due: "09/01/1997",
                    uuid: getUuid()

                },
                {
                    name: "Wide Open Spaces",
                    number: 1998,
                    amount: "$4,600",
                    due: "01/27/1998",
                    uuid: getUuid()

                },
                {
                    name: "Monica",
                    number: 1995,
                    amount: "$10,800",
                    due: "12/05/1995",
                    uuid: getUuid()
                },
            ],
            pagination: {
                pageNum: 1,
                pageSize: 10,
            },
            selectedRows: [],
            drawerVisible: false,
            drawertitle: '编辑',
            invoiceDetailStatus: INVOICE_DETAIL_STATUS.VIEW
        }
    }

    handleViewInvoice = (record) => {
        this.setState({
            selectedRows: record,
            drawerVisible: true,
            drawertitle: '查看发票详细信息',
            invoiceDetailStatus: INVOICE_DETAIL_STATUS.VIEW,
        });
    }
    handleEditInvoice = (record) => {
        this.setState({
            selectedRows: record,
            drawerVisible: true,
            drawertitle: '修改发票信息',
            invoiceDetailStatus: INVOICE_DETAIL_STATUS.EDIT,
        });
    }
    createAction = (record) => {
        return (
            <>
                <div>
                    <Tooltip title='查看详情'>
                        <a
                            onClick={() => this.handleViewInvoice(record)}
                        >
                            <Icon type='eye'/>
                        </a>
                    </Tooltip>
                    <Divider type='vertical'/>
                    <Tooltip title='编辑'>
                        <a
                            onClick={() => this.handleEditInvoice(record)}
                        >
                            <Icon type='edit'/>
                        </a>
                    </Tooltip>
                </div>

            </>
        )
    }
    closeDrawer = () => {
        this.setState({
            drawerVisible: false
        })
    }

    render() {
        const {invoiceRecords = [], pagination = {}} = this.state;
        const {
            selectedRows,
            drawerVisible,
            drawertitle,
            invoiceDetailStatus
        } = this.state;
        const data = {
            list: invoiceRecords,
            pagination
        };
        const columns = [
            {
                title: 'uuid',
                key: 'uuid',
                dataIndex: 'uuid',
                width: '20%',
                align: 'center',
            },
            {
                title: '序号',
                key: 'index',
                dataIndex: 'index',
                width: '10%',
                align: 'center',
                render: (text, record, index) => <span>{index + 1}</span>
            },
            {
                title: '姓名',
                key: 'name',
                dataIndex: 'name',
                width: '10%',
            }, {
                title: '数量',
                key: 'number',
                dataIndex: 'number',
                width: '10%',
            },
            {
                title: '总价',
                key: 'amount',
                dataIndex: 'amount',
                width: '10%',
            }, {
                title: '发票日期',
                key: 'due',
                dataIndex: 'due',
                width: '10%',
            }, {
                title: 'Action',
                key: 'action',
                align: 'center',
                width: '20%',
                render: (text, record) => (
                    <div>
                        {this.createAction(record)}
                    </div>
                )
            }
        ];
        const contentOption = {
            onClose: this.closeDrawer,
            // editFunc: this.editInvoice,
            invoiceDetailStatus,
        };
        const drawerOption = {
            onClose: this.closeDrawer,
            drawertitle,
            drawerVisible,
            drawerContent: <InvoiceDetail data={selectedRows} {...contentOption} />
        };
        return (
            <>
                <StandardTable
                    selectedRows={selectedRows}
                    onSelectRow={() => {
                    }}
                    rowSelection={null}
                    data={data}
                    columns={columns}
                    onChange={() => {
                    }}
                    rowKey={r => r.uuid}
                />
                <AdvancedDrawer {...drawerOption} />
            </>
        );
    }
}

export default Invoice;
