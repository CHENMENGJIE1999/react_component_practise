import React from "react";
import {Form, Row, Col, Button, Card, Descriptions, InputNumber, Icon, Input} from 'antd';
import {INVOICE_DETAIL_STATUS} from "./utlis/Enum";
const FormItem = Form.Item;

class InvoiceDetail extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {invoiceDetailStatus, onClose, data} = this.props;
        let DETAIL_CONTENT = {};
        const {name = '', number = '', amount = '', due = ''} = data;
        switch (invoiceDetailStatus) {
            case INVOICE_DETAIL_STATUS.VIEW:
                DETAIL_CONTENT = (
                    <Descriptions bordered style={{marginBottom: "2em"}}>
                        <Descriptions.Item label="选手id">
                            {name || ''}
                        </Descriptions.Item>
                        <Descriptions.Item label="年龄">
                            {number || ''}
                        </Descriptions.Item>
                        <Descriptions.Item span={1} label="真实姓名">
                            {amount || ''}
                        </Descriptions.Item>
                        <Descriptions.Item span={1} label="昵称">
                            {due || ''}
                        </Descriptions.Item>
                    </Descriptions>
                );
                break;
            case INVOICE_DETAIL_STATUS.EDIT:
                DETAIL_CONTENT = (
                    <div>
                        <Form className="login-form">
                            <FormItem label='数量'>
                                <InputNumber
                                    min={0}
                                    max={5}
                                    step={0.5}
                                />
                            </FormItem>
                        </Form>
                        <Form className="login-form">
                            <FormItem label='总价'>
                                <InputNumber
                                    min={0}
                                    max={5}
                                    step={0.5}
                                />
                            </FormItem>
                        </Form>
                    </div>
                );
                break;
            default:
                break;
        }
        return (
            <>
                {DETAIL_CONTENT}
            </>
        )
    }
}

export default InvoiceDetail;
