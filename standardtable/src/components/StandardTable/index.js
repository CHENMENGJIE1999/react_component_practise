/** 版本号:1.0
创建时间:2022/3/23
创建人:陈梦洁 **/

import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Table, Alert } from 'antd';
import styles from './index.less';

function initTotalList(columns) {
  const totalList = [];
  columns.forEach(column => {
    if (column.needTotal) {
      totalList.push({ ...column, total: 0 });
    }
  });
  return totalList;
}

class StandardTable extends PureComponent {

  static propTypes = {
    data: PropTypes.object.isRequired, //下拉列表框的数据源
    columns: PropTypes.array.isRequired, //表格列的配置描述
    onChange: PropTypes.func.isRequired, //发生改变时的操作
  };

  constructor(props) {
    super(props);
    const { columns } = props;
    const needTotalList = initTotalList(columns);
    this.state = {
      selectedRowKeys: [],
      needTotalList,
    };
  }
  // 将传入的props映射到state上面，每次render之前调用，即使props没有变化，但是父state变化，导致子组件re-render，生命周期函数依然被调用
  static getDerivedStateFromProps(nextProps) {
    // clean state
    if (nextProps.selectedRows.length === 0) {
      const needTotalList = initTotalList(nextProps.columns);
      return {
        selectedRowKeys: [],
        needTotalList,
      };
    }
    // props传入的内容不需影响state，返回一个null
    return null;
  }

  handleRowSelectChange = (selectedRowKeys, selectedRows) => {
    let { needTotalList } = this.state;
    needTotalList = needTotalList.map(item => ({
      ...item,
      total: selectedRows.reduce((sum, val) => sum + parseFloat(val[item.dataIndex], 10), 0),
    }));
    const { onSelectRow } = this.props;
    if (onSelectRow) {
      onSelectRow(selectedRows);
    }
    
    this.setState({ selectedRowKeys, needTotalList });
  };

  handleTableChange = (pagination, filters, sorter) => {
    const { onChange } = this.props;
    if (onChange) {
      onChange(pagination, filters, sorter);
    }
  };

  handleRowClick = (record) => {
    const { onRowClick } = this.props;
    if (onRowClick) {
      onRowClick(record);
    }
  };

  cleanSelectedKeys = () => {
    this.handleRowSelectChange([], []);
  };

  render() {
    const { selectedRowKeys, needTotalList } = this.state;
    const { data = {}, alertDisplay, rowKey, ...rest } = this.props;
    const { list = [], pagination } = data;
    let paginationProps;
    if (JSON.stringify(pagination) === '{}') {
      paginationProps = false;
    } else {
      paginationProps = {
        showSizeChanger: true,
        showQuickJumper: true,
        ...pagination,
      };
    }

    const rowSelection = {
      selectedRowKeys,
      onChange: this.handleRowSelectChange,
      getCheckboxProps: record => ({
        disabled: record.disabled,
      }),
    };
    paginationProps = { ...paginationProps, current: paginationProps.pageNum };
    return (
      <div className={styles.standardTable}>
        {
          this.props.rowSelection === null ? '' : <div className={styles.tableAlert} style={{ display: alertDisplay || '' }}>
            <Alert
              message={
                <Fragment>
                  已选择 <a style={{ fontWeight: 600 }}>{selectedRowKeys.length}</a> 项&nbsp;&nbsp;
                  {needTotalList.map(item => (
                    <span style={{ marginLeft: 8 }} key={item.dataIndex}>
                      {item.title}
                      总计&nbsp;
                      <span style={{ fontWeight: 600 }}>
                        {item.render ? item.render(item.total) : item.total}
                      </span>
                    </span>
                  ))}
                  <a onClick={this.cleanSelectedKeys} style={{ marginLeft: 24 }}>
                    清空
                  </a>
                </Fragment>
              }
              type="info"
              showIcon
            />
          </div>
        }

        <Table
          rowKey={rowKey || 'key'}
          rowSelection={rowSelection}
          dataSource={list}
          pagination={paginationProps}
          onChange={this.handleTableChange}
          onRow={record => ({ onClick: this.handleRowClick.bind(this, record) })}
          {...rest}
        />
      </div>
    );
  }
}

export default StandardTable;
