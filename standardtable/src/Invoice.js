import React from 'react';
import { getUuid } from './utlis/utlis';
import StandardTable from './components/StandardTable/index'
class Invoice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            invoiceRecords: [
                {
                    name: "Santa Monica",
                    number: 1995,
                    amount: "$10,800",
                    due: "12/05/1995",
                    uuid: getUuid()
                },
                {
                    name: "Stankonia",
                    number: 2000,
                    amount: "$8,000",
                    due: "10/31/2000",
                    uuid: getUuid()

                },
                {
                    name: "Ocean Avenue",
                    number: 2003,
                    amount: "$9,500",
                    due: "07/22/2003",
                    uuid: getUuid()

                },
                {
                    name: "Tubthumper",
                    number: 1997,
                    amount: "$14,000",
                    due: "09/01/1997",
                    uuid: getUuid()

                },
                {
                    name: "Wide Open Spaces",
                    number: 1998,
                    amount: "$4,600",
                    due: "01/27/1998",
                    uuid: getUuid()

                },
                {
                    name: "Monica",
                    number: 1995,
                    amount: "$10,800",
                    due: "12/05/1995",
                    uuid: getUuid()
                },
            ],
            pagination: {
                pageNum: 1,
                pageSize: 10,
            },
            selectedRows: [],
        }
    }
    handleSelectRows = (val) => {
        this.setState({
            selectedRows: val
        })
    }
    render() {
        const { invoiceRecords = [], pagination = {}, selectedRows = [], } = this.state;
        const data = {
            list: invoiceRecords,
            pagination
        };
        const columns = [
            {
                title: 'uuid',
                key: 'uuid',
                dataIndex: 'uuid',
                width: '30%',
                align: 'center',
            },
            {
                title: '序号',
                key: 'index',
                dataIndex: 'index',
                width: '10%',
                align: 'center',
                render: (text, record, index) => <span>{index + 1}</span>
            },
            {
                title: '姓名',
                key: 'name',
                dataIndex: 'name',
                width: '20%',
            }, {
                title: '数量',
                key: 'number',
                dataIndex: 'number',
                width: '10%',
            },
            {
                title: '总价',
                key: 'amount',
                dataIndex: 'amount',
                width: '10%',
            }, {
                title: '发票日期',
                key: 'due',
                dataIndex: 'due',
                width: '20%',
            },
        ];
        return (
            <>
                <StandardTable
                    selectedRows={selectedRows}
                    onSelectRow={this.handleSelectRows}
                    // rowSelection={null}
                    data={data}
                    columns={columns}
                    onChange={() => { }}
                    rowKey={r => r.uuid}
                />

            </>
        );
    }
}

export default Invoice;