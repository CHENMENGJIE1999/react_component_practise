# StandardTable组件使用
## 实现功能

传入数据及相关配置，实现数据以表格的形式显示

效果如下所示

![1649399107057](assets/1649399107057.png)详情请见组件下`Readme.md`文件。

## 知识点

- 组件传值
- 列表渲染
- key

## 注意事项

PS：`react`和`antd1`分别使用以下版本进行开发,否则可能出现意想不到的错误

```js
npm add react@^16.13.0 react-dom@^16.13.0

npm install --save antd@3.11.6
```

## 参考文档

react官网

https://reactjs.org/blog/2020/02/26/react-v16.13.0.html

antd官网

https://ant.design/docs/react/use-with-create-react-app-cn

